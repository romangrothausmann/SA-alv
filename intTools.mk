
### default paths of external libraries are expected from main Makefile
ITKLIB?=""
VTKLIB?=""

## path to submodules
export SUBDIR = $(realpath submodules)

### setting default paths of internal programs
ITK?=$(SUBDIR)/ITK-CLIs/
VTK?=$(SUBDIR)/VTK-CLIs/
FA?=$(SUBDIR)/FacetAnalyser/


.PHONY: all clean


all : $(ITKEXE)
all : $(VTKEXE)
all : $(FAEXE)
clean :
	$(MAKE) -C $(ITK)/build/ clean
	$(MAKE) -C $(VTK)/build/ clean
	$(MAKE) -C $(FA)/build/ clean

.PHONY: initITK
initITK :
	mkdir -p $(ITK)/build/ && \
	cd $(ITK)/build/ && \
	cmake -DITK_DIR=$(ITKLIB) -DCMAKE_BUILD_TYPE=Release ..

$(ITKEXE) : initITK
	$(MAKE) -C $(ITK)/build/ $@

.PHONY: initVTK
initVTK :
	mkdir -p $(VTK)/build/ && \
	cd $(VTK)/build/ && \
	cmake -DVTK_DIR=$(VTKLIB) -DCMAKE_BUILD_TYPE=Release ..

$(VTKEXE) : initVTK
	$(MAKE) -C $(VTK)/build/ $@

.PHONY: initFA
initFA : # needs ITK with ITKVtkGlue, so ITK will serve VTK_DIR
	mkdir -p $(FA)/build/ && \
	cd $(FA)/build/ && \
	cmake \
		-DITK_DIR=$(ITKLIB) \
		-DBUILD_PLUGIN=OFF \
		-DBUILD_TESTING=OFF \
		-DBUILD_EXAMPLE=ON \
		-DCMAKE_BUILD_TYPE=Release \
		../code/

$(FAEXE) : initFA
	$(MAKE) -C $(FA)/build/ $@
